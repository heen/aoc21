use std::path::Path;

fn main() {
    println!("Hello, world!");
    a1();
    b1();
}

fn file_to_ints(f: &Path) -> Vec<u32> {
    let filestring = std::fs::read_to_string(f).expect("failed to read file");
    let words = filestring.split_ascii_whitespace();
    let nums: Vec<u32> = words
        .filter(|x| x.len() > 0)
        .map(|x| x.parse().expect("Failed to read num"))
        .collect();
    nums
}

fn a1() {
    let puzzle_input = Path::new("input/1.txt");
    let depths = file_to_ints(puzzle_input);
    // let depths = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
    let mut drops = 0;
    for i in 0..depths.len() - 1 {
        if depths[i] < depths[i + 1] {
            drops = drops + 1;
        }
    }
    println!("1A: {} depth steps", drops);
}

fn b1() {
    let puzzle_input = Path::new("input/1.txt");
    let depths = file_to_ints(puzzle_input);
    // let depths = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
    let mut last_triple = depths[0] + depths[1] + depths[2];
    let mut drops = 0;
    for i in 1..depths.len() - 2 {
        let new_drop = depths[i] + depths[i + 1] + depths[i + 2];
        if new_drop > last_triple {
            drops = drops + 1;
        }
        last_triple = new_drop;
    }
    println!("1B: {} depth steps", drops);
}
